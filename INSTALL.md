Executive summary
=================

Building vfplot is quite standard: the usual

    ./configure
    make
    su
    make install

Use the `--prefix` option to `./configure` if you want to install
elsewhere than `/usr/local`.

See `./configure --help` for a full list of configuration options.

Optional features
=================

If the [NetCDF](http://www.unidata.ucar.edu/software/netcdf/) library is
found then vfplot will be able to read GMT grd files.

If the [matio](http://matio.sourceforge.net/) library is found then
vfplot will be able to read mat (Matlab binary) files.

If you want support for [Gerris](http://gfs.sourceforge.net/) flow
solver files use the `--enable-gerris` option in configuration. You will
need to have a recent version of the Gerris software.

If the pthread (POSIX thread) library is found then vfplot will support
multithreaded force calculations giving a substantial speedup on
multiprocessor hardware.

If the [zlib](http://www.zlib.net/) library is found then the graphics
state files will be compressed with gzip (see the `--graphic-state`
option).

Platform specific notes
=======================

The program should compile on any modern POSIX operating system with a
C99 compiler. It is developed on Linux on the x86, AMD64 and Sparc
architectures with the GNU C compiler.

Windows XP with Cygwin
----------------------

The program is tested on Windows XP with the
[Cygwin](http://www.cygwin.com/) POSIX compatibility layer.

To build, first use the Cygwin startup.exe program to install the gcc,
make and zlib pacakges. Then obtain, compile and install the netcdf and
libmatio libraries listed above. Finally, configure with
`--disable-pthread` then make and install as usual.

OS X
----

The POSIX thread (pthread) library supplied with the OS does not include
some functionality on which vfplot relies (in particular, *barriers*).
That functionality can be provided by a built-in implementation with the
`--enable-pthread-extra` configuration option.
