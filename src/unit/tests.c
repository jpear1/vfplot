/*
  tests.c
  testcase loader
  J.J.Green 2007
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "test_arrow.h"
#include "test_aspect.h"
#include "test_bbox.h"
#include "test_bilinear.h"
#include "test_contact.h"
#include "test_cubic.h"
#include "test_curvature.h"
#include "test_domain.h"
#include "test_ellipse.h"
#include "test_margin.h"
#include "test_matrix.h"
#include "test_polyline.h"
#include "test_polynomial.h"
#include "test_potential.h"
#include "test_sagread.h"
#include "test_sagwrite.h"
#include "test_units.h"
#include "test_vector.h"

#include "cunit_compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("arrows", tests_arrow),
    ENTRY("aspect ratio", tests_aspect),
    ENTRY("bounding boxes", tests_bbox),
    ENTRY("bilinear interpolant", tests_bilinear),
    ENTRY("contact", tests_contact),
    ENTRY("cubic", tests_cubic),
    ENTRY("curvature", tests_curvature),
    ENTRY("domain", tests_domain),
    ENTRY("ellipse", tests_ellipse),
    ENTRY("margin", tests_margin),
    ENTRY("matrix", tests_matrix),
    ENTRY("polyline", tests_polyline),
    ENTRY("polynomial", tests_polynomial),
    ENTRY("potential", tests_potential),
    ENTRY("reading SAG", tests_sagread),
    ENTRY("writing SAG", tests_sagwrite),
    ENTRY("units", tests_units),
    ENTRY("vector", tests_vector),
    CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,"suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
