# -*- makefile -*-
# package documentation

XSLBASE = http://docbook.sourceforge.net/release/xsl/current
MANXSL = $(XSLBASE)/manpages/docbook.xsl
XHTMLXSL = $(XSLBASE)/xhtml/docbook.xsl

%.md: %.xml
	pandoc -s -f docbook -t markdown $< -o $@

%.1: %.xml ../docbook
	xsltproc --xinclude $(MANXSL) $<

%.5: %.xml ../docbook
	xsltproc --xinclude $(MANXSL) $<
