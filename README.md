<!-- This is the README for Github -->

vfplot
======

A program for plotting two-dimensional vector fields. For detailed
documentation and stable releases see the [vfplot homepage][1].

To build from git sources run the script `script/setup.sh`.

[1]: http://soliton.vm.bytemark.co.uk/pub/jjg/en/code/vfplot/
